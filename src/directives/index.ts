import { DirectiveOptions } from 'vue';

export const clickOutside: DirectiveOptions = {
  inserted(el: any, binding, vnode) {
    const vm = vnode.context;
    const callback = binding.value;

    el.clickOutsideEvent = (event: MouseEvent) => {
      // el-select-dropdown, el-picker-panel Костыль для element.io
      const target = event.target as HTMLElement;

      if (
        !(
          el === target ||
          el.contains(target) ||
          target.closest('.el-select-dropdown') ||
          target.closest('.el-picker-panel')
        )
      ) {
        return callback.call(vm, event);
      }
    };

    document.addEventListener('mousedown', el.clickOutsideEvent);
  },
  unbind(el: any) {
    document.removeEventListener('mousedown', el.clickOutsideEvent);
  },
};
