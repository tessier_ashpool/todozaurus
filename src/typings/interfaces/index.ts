export interface ITask {
  label: string;
  isDone: boolean;
}

export interface IState {
  tasks: ITask[];
}
