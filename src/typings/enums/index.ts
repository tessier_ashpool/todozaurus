export enum ActionsEnum {
  ADD_TODO = 'ADD_TODO',
  REMOVE_TODO = 'REMOVE_TODO',
}

export enum MutationsEnum {
  ADD_TODO = 'ADD_TODO',
  REMOVE_TODO = 'REMOVE_TODO',
  UPDATE_TODO = 'UPDATE_TODO',
}

export enum GettersEnum {
  GET_TODOS = 'GET_TODOS',
}
