import Vue from 'vue';
import Vuex from 'vuex';

import { ITask, IState } from '@/typings/interfaces';
import { ActionsEnum, GettersEnum, MutationsEnum } from '@/typings/enums';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    tasks: [
      { label: 'Построить дом', isDone: false },
      { label: 'Написать автопортрет', isDone: false },
    ],
  } as IState,
  mutations: {
    [MutationsEnum.ADD_TODO](state, task: ITask) {
      state.tasks.push(task);
    },
    [MutationsEnum.UPDATE_TODO](state, task: ITask) {
      const currTask: ITask | undefined = state.tasks.find((t: ITask) => t === task);
      if (currTask) {
        currTask.isDone = !currTask.isDone;
      }
    },
    [MutationsEnum.REMOVE_TODO](state, task: ITask) {
      const index = state.tasks.indexOf(task);
      state.tasks.splice(index, 1);
    },
  },
  actions: {
    [ActionsEnum.ADD_TODO]({ commit }, task: ITask) {
      commit(MutationsEnum.ADD_TODO, task);
    },
  },
  getters: {
    [GettersEnum.GET_TODOS](state) {
      return state.tasks;
    },
  },
});
